import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ZoneComponent } from './zone/zone.component'
import { RoomComponent } from './room/room.component'
import { NotFoundComponent } from './not-found/not-found.component'

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'zone/:id',
    component: ZoneComponent
  },
  {
    path: 'zone',
    component: ZoneComponent
  },
  {
    path: 'room/:id',
    component: RoomComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
