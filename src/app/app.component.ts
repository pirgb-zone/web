import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ApiService } from './_services/api.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'pirgb-zone-web';
  create: String = 'zone';
  zone_name: String;
  zone_pin: Object = {'r': null, 'g': null, 'b': null}


  constructor(private modal: NgbModal, private api: ApiService) {}

  open_modal(contend) {
    this.modal.open(contend, {size: 'lg'})
    this.zone_pin['r'] = null
    this.zone_pin['g'] = null
    this.zone_pin['b'] = null
    this.zone_name = ''
  }

  create_zone() {
    var zone = {}
    zone["name"] = this.zone_name
    zone["pin_red"] = this.zone_pin["r"]
    zone["pin_green"] = this.zone_pin["g"]
    zone["pin_blue"] = this.zone_pin["b"]
    this.api.zone.push(zone)
    console.log(this.api.zone)
  }
}
