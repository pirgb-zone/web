import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ColorPickerService, ColorPickerModule } from 'ngx-color-picker'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ZoneComponent } from './zone/zone.component';
import { HomeComponent } from './home/home.component';
import { RoomComponent } from './room/room.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { ApiService } from './_services/api.service'

@NgModule({
  declarations: [
    AppComponent,
    ZoneComponent,
    HomeComponent,
    RoomComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ColorPickerModule,
    NgbModule,
    AngularFontAwesomeModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ColorPickerService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
