import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  api: String = '/api/'
  zone: Array<Object> = []

  constructor(private _http: HttpClient) { }

  get(url){
    return this._http.get(this.api + url)
      .pipe(map(res => res))
  }

  post(url, data){
    return this._http.post(this.api + url, data=data)
      .pipe(map(res => res))
  }

  put(url, data){
    return this._http.put(this.api + url, data=data)
      .pipe(map(res => res))
  }

  //delete


  //zone

  get_zones(){
    return this.get('zone')
  }

  get_zone(id: number){
    return this.get('zone/' + id)
  }

  zone_change_color(id: number, r: string, g: string, b: string){
    let par = '?r=' + r + '&g=' + g + '&b=' + b
    return this.put('zone/' + id + '/color' + par, null)
  }
}
