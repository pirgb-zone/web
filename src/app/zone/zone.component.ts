import { Component, OnInit } from '@angular/core';
import { ApiService } from '../_services/api.service'
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-zone',
  templateUrl: './zone.component.html',
  styleUrls: ['./zone.component.scss']
})
export class ZoneComponent implements OnInit {

  id: number
  zones: Array<Object> = []
  zone: Object
  list: Boolean
  color: string

  constructor(private api: ApiService, private route: ActivatedRoute) {
    this.route.paramMap
      .subscribe(res => {
        if(res.get('id') === null){
          this.list = true
        } else {
          this.list = false
          this.id =+ res.get('id')
        }
      })
  }

  ngOnInit() {
    if(this.id){
      this.api.get_zone(this.id)
        .subscribe(res => {
          let r = res['colors']['r']
          let g = res['colors']['g']
          let b = res['colors']['b']
          this.color = "rgb(" + r + ',' + g + ',' + b + ')'
          this.zone = res
        })
    } else {
      this.api.get_zones()
        .subscribe(res => {
          this.zones = res["results"]
        })
    }
  }

  save(){
    let rgb
    rgb = this.color.split('(')[1].split(')')[0].split(',')
    this.api.zone_change_color(this.id, rgb[0], rgb[1], rgb[2])
      .subscribe(res => {
        this.zone = res
      })
  }

}
